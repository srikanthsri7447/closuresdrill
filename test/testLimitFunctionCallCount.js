const limitFunctionCallCount = require("../limitFunctionCallCount");

function add(a, b) {
    return a + b;
}

function doMath(a, b, c) {
    return a + b - c;
}


// passing Add cb function as an argument

const limitedAddFn = limitFunctionCallCount(add, 2);

console.log(limitedAddFn(1, 2)); // 3
console.log(limitedAddFn(3, 4)); // 7
console.log(limitedAddFn(4, 5)); // null


// passing doMath cb function as an argument

const limitedMathFn = limitFunctionCallCount(doMath, 3);

console.log(limitedMathFn(1, 2, 3)); // 0
console.log(limitedMathFn(3, 4, 5)); // 2
console.log(limitedMathFn(4, 5, 1)); // 8
console.log(limitedMathFn(4, 5, 6)); // null