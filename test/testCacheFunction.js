const cacheFunction = require("../cacheFunction");

function add(a, b) {
    return a + b;
}

function square(num) {
    return num * num;
}


function doMath(a, b, c) {
    return a + b - c;

}

// Adding numbers cb function as an argument

const cachedAdd = cacheFunction(add);

console.log(cachedAdd(1, 2)); // add should be invoked
console.log(cachedAdd(1, 2)); // add should not be invoked but the same result returned from the cache
console.log(cachedAdd(2, 1)); // add should be invoked
console.log(cachedAdd(2, 3)); // add should be invoked
console.log(cachedAdd(2, 3)); // add should not be invoked but the same result returned from the cache

// Domath of the numbers cb function as an argument


const cachedDoMath = cacheFunction(doMath);

console.log(cachedDoMath(1, 2, 3)); // doMath should be invoked
console.log(cachedDoMath(1, 2, 3)); // doMath should not be invoked but the same result returned from the cache
console.log(cachedDoMath(1, 3, 2)); // doMath should be invoked
console.log(cachedDoMath(2, 3, 4)); // doMath should be invoked
console.log(cachedDoMath(2, 3, 4)); // doMath should not be invoked but the same result returned from the cache

//square of the number cb function as an argument

const cachedSquare = cacheFunction(square);

console.log(cachedSquare(2)); // square should be invoked
console.log(cachedSquare(2)); // square should not be invoked but the same result returned from the cache
console.log(cachedSquare(3)); // square should be invoked
console.log(cachedSquare(3)); // square should not be invoked but the same result returned from the cache

