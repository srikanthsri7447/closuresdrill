function limitFunctionCallCount(cb, n) {

    let count = 0;

    return function (...arg) {
        if (count < n) {
            count = count + 1
            return cb(...arg)
        }
        else {
            return null;
        }

    }


}



module.exports = limitFunctionCallCount;