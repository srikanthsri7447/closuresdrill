function cacheFunction(cb) {

    const cache = {}

    return function (...arg) {

        if (!cache[`${cb} of ${arg}`]) {

            cache[`${cb} of ${arg}`] = cb(...arg)

            return cb(...arg)
        }
        else {

            return cache[`${cb} of ${arg}`]
        }
    }

}

module.exports = cacheFunction;